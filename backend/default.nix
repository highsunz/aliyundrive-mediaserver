{ version, rustPlatform }: rustPlatform.buildRustPackage {
  pname = "aliyundrive-mediaserver-backend";
  inherit version;
  src = ./.;
  cargoLock.lockFile = ./Cargo.lock;
}
