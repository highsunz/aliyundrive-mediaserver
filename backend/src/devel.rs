use crate::data;

#[allow(dead_code)]
pub(crate) fn get_qr_data_internal() -> anyhow::Result<Option<data::QrData>> {
    Ok(Some(data::QrData {
        qr_code_url: "https://openapi.aliyundrive.com/oauth/qrcode/ffffffffffffffffffffffffffffffff".to_string(),
        sid: "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx".to_string(),
    }))
}

#[allow(dead_code)]
pub(crate) fn query_login_status_internal(sid: String) -> anyhow::Result<Option<String>> {
    tracing::info!("sid: {}", sid);
    Ok(Some("cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc".to_string()))
}
