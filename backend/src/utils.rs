use std::{fmt, fs, io, path, sync, time};
use tokio::process;

use axum::{http::StatusCode, response::IntoResponse};

use crate::data;

const ALIYUNDRIVE_WEBDAV_PROGRAM: &str = "aliyundrive-webdav";

pub(crate) mod helpers {
    use crate::data;
    use std::{fs, io, path};

    pub(crate) fn kill_batch_pids(pids: &[u32]) -> bool {
        // first force iterators to be evaluated (instead of calling the .all method), or a failure
        // will abort the iterator, causing the later processes not being killed.
        let kill_results: Vec<bool> = pids
            .iter()
            .map(|pid| {
                nix::sys::signal::kill(
                    nix::unistd::Pid::from_raw(*pid as i32),
                    nix::sys::signal::Signal::SIGTERM,
                )
                .is_ok()
            })
            .collect();
        // using .all here is fine
        kill_results.into_iter().all(|x| x)
    }

    pub(crate) fn is_healthy(model: &tokio::sync::MutexGuard<data::Model>, nick: &str) -> bool {
        if let Some(mounted_client) = model.mounted_clients.get(nick) {
            if let Ok(status) = mounted_client
                .pids
                .iter()
                .map(|pid| path::Path::new("/proc").join(pid.to_string()).try_exists())
                .collect::<Result<Vec<bool>, _>>()
            {
                status.into_iter().all(|x| x)
            } else {
                false
            }
        } else {
            false
        }
    }

    pub(crate) fn create_rclone_cfg<P>(p: P, nick: &str, port: &u16) -> anyhow::Result<()>
    where
        P: AsRef<path::Path>,
    {
        fs::write(
            p,
            format!(
                r#"[{}]
type = webdav
url = http://127.0.0.1:{}
vendor = nextcloud
nextcloud_chunk_size = 0
"#,
                // set nextcloud_chunk_size=0 to avoid rclone checking webdav url
                // REF: <https://github.com/rclone/rclone/issues/7103>
                //       <https://github.com/messense/aliyundrive-webdav/issues/889>
                nick,
                port
            ),
        )?;
        Ok(())
    }

    fn port_occupied(port: &u16) -> bool {
        use std::net::TcpListener;
        TcpListener::bind(("127.0.0.1", *port)).is_err()
    }

    fn port_available(port: &u16) -> bool {
        !port_occupied(port)
    }

    pub(crate) fn find_port(lo: u16, hi: u16) -> Option<u16> {
        (lo..hi).find(port_available)
    }

    pub(crate) fn get_refresh_token_for(
        nick: &str,
        states: &[data::Client],
    ) -> Option<data::RefreshToken> {
        let state: Vec<Option<data::RefreshToken>> = states
            .iter()
            .filter(|s| s.info.nick == nick)
            .map(|s| s.refresh_token.clone())
            .collect();
        if state.len() == 1 {
            state[0].clone()
        } else {
            None
        }
    }

    pub(crate) fn get_mount_point_for(nick: &str, cfg: &data::Opt) -> path::PathBuf {
        cfg.mount_dir.join(nick)
    }

    pub fn check_and_create_dir<P, S>(dir: P, pretty_name: S) -> anyhow::Result<()>
    where
        P: AsRef<path::Path>,
        S: AsRef<str>,
    {
        if !dir.as_ref().exists() {
            tracing::info!(
                "creating {} at {}",
                pretty_name.as_ref(),
                dir.as_ref().display()
            );
            std::fs::create_dir_all(dir.as_ref())?;
            Ok(())
        } else if !dir.as_ref().is_dir() {
            let error = format!(
                "{} ({}) is not a directory",
                pretty_name.as_ref(),
                dir.as_ref().canonicalize()?.display()
            );
            tracing::error!(error);
            // TODO: unstable library feature 'io_error_more'
            // REF: <https://github.com/rust-lang/rust/issues/86442>
            //
            // io::ErrorKind::NotADirectory,
            Err(io::Error::new(io::ErrorKind::Other, error).into())
        } else {
            Ok(())
        }
    }

    pub fn check_nick_availability<S>(clients: &[data::Client], nick: S) -> bool
    where
        S: AsRef<str>,
    {
        !clients
            .iter()
            .any(|client| client.info.nick == nick.as_ref())
    }

    fn get_mount<P>(mountpoint: P) -> anyhow::Result<Option<proc_mounts::MountInfo>>
    where
        P: AsRef<path::Path>,
    {
        let mi: Vec<proc_mounts::MountInfo> = proc_mounts::MountIter::new()?
            .filter(|mi| mi.is_ok() && mi.as_ref().unwrap().dest == mountpoint.as_ref())
            .collect::<io::Result<Vec<_>>>()?;
        if mi.len() == 1 {
            Ok(Some(mi[0].clone()))
        } else if mi.is_empty() {
            Ok(None)
        } else if mi.len() > 1 {
            Err(io::Error::new(io::ErrorKind::Other, "Found multiple mount points?").into())
        } else {
            unreachable!()
        }
    }

    pub fn is_fuse<P>(mountpoint: P) -> bool
    where
        P: AsRef<path::Path>,
    {
        match get_mount(mountpoint) {
            Ok(Some(mi)) => mi.fstype.contains("fuse"),
            _ => false,
        }
    }
}

pub(crate) async fn get_qr_data_internal() -> anyhow::Result<Option<data::QrData>> {
    if let Ok(qr_data_output) = process::Command::new(ALIYUNDRIVE_WEBDAV_PROGRAM)
        .env("RUST_LOG", "error")
        .args(["qr", "generate"])
        .output()
        .await
    {
        let qr_data: data::QrData =
            serde_json::from_str(&String::from_utf8(qr_data_output.stdout)?)?;
        // dbg!(&qr_data);
        Ok(Some(qr_data))
    } else {
        tracing::error!(
            "Failed to get qr data, was '{}' installed?",
            ALIYUNDRIVE_WEBDAV_PROGRAM
        );
        Ok(None)
    }
}

pub(crate) async fn query_login_status_internal<S>(sid: S) -> anyhow::Result<Option<String>>
where
    S: AsRef<str> + fmt::Display,
{
    if let Ok(login_query_output) = process::Command::new(ALIYUNDRIVE_WEBDAV_PROGRAM)
        .env("RUST_LOG", "error")
        .args(["qr", "query", "--sid", &sid.to_string()])
        .output()
        .await
    {
        let refresh_token_bytes = login_query_output.stdout;

        match data::RefreshToken::from_bytes(&refresh_token_bytes) {
            Ok(refresh_token) => {
                tracing::debug!("Login success, refresh token: {}", refresh_token);
                Ok(Some(refresh_token.token()))
            }
            _ => {
                // The sid is not known or not logged in
                tracing::debug!("Not logged in (sid: {})", sid);
                Ok(None)
            }
        }
    } else {
        Err(io::Error::new(io::ErrorKind::Other, "Login query failed").into())
    }
}

pub(crate) fn load_state_file<P>(p: P) -> anyhow::Result<Vec<data::Client>>
where
    P: AsRef<path::Path>,
{
    if p.as_ref().exists() && p.as_ref().is_file() {
        let states = serde_json::from_str(&String::from_utf8(fs::read(p)?)?)?;
        Ok(states)
    } else {
        Ok(vec![])
    }
}

pub(crate) fn mount_drive<P>(
    refresh_token: Option<data::RefreshToken>,
    mountpoint: P,
    tmpdir: P,
) -> anyhow::Result<(u16, Vec<u32>)>
where
    P: AsRef<path::Path>,
{
    // NOTE: set `user_allow_other` in /etc/fuse.conf (programs.fuse.userAllowOther on nixos) to
    // allow unprivileged mounting.  Using sudo here will cause problems when gracefully killing
    // the drive process later.
    if !mountpoint.as_ref().exists() {
        std::fs::create_dir(mountpoint.as_ref())?;
    }
    if let Some(refresh_token) = refresh_token {
        let available_port = helpers::find_port(19800, 20000);
        if available_port.is_none() {
            return Err(io::Error::new(io::ErrorKind::Other, "No available port found").into());
        }
        let available_port = available_port.unwrap();

        // process id for aliyundrive-webdav
        let child = process::Command::new(ALIYUNDRIVE_WEBDAV_PROGRAM)
            // .env("RUST_LOG", "error")
            .env("REFRESH_TOKEN", &refresh_token.token())
            .args([
                "--read-only",
                "--host",
                "127.0.0.1",
                "--port",
                &available_port.to_string(),
            ])
            .spawn()?;

        let mut pids = Vec::new();
        match child.id() {
            Some(pid) => {
                tracing::debug!(
                    pid,
                    "Successfully launched {} with",
                    ALIYUNDRIVE_WEBDAV_PROGRAM
                );
                pids.push(pid);
            }
            None => {
                let msg = format!("Got no pid, {} did not start", ALIYUNDRIVE_WEBDAV_PROGRAM);
                tracing::error!(msg);
                return Err(io::Error::new(io::ErrorKind::Other, msg).into());
            }
        }

        let nick = mountpoint.as_ref().file_name().unwrap().to_str().unwrap();
        let cfg_file = tmpdir.as_ref().join(format!("{}.conf", &nick));
        helpers::create_rclone_cfg(&cfg_file, &nick, &available_port)?;

        // rclone
        let child = process::Command::new("rclone")
            .args([
                "--config",
                cfg_file.to_str().unwrap(),
                "mount",
                // // allow mounting upon a mounted directory
                // "--allow-non-empty",
                &format!(
                    "{}:/",
                    mountpoint.as_ref().file_name().unwrap().to_str().unwrap()
                ),
                mountpoint.as_ref().to_str().unwrap(),
            ])
            .spawn()?;
        match child.id() {
            Some(pid) => {
                tracing::debug!(
                    pid,
                    "Successfully launched rclone for mounting drive at '{}' with",
                    mountpoint.as_ref().display(),
                );
                pids.push(pid);
            }
            None => {
                let msg = "Got no pid, rclone did not start";
                tracing::error!(msg);
                return Err(io::Error::new(io::ErrorKind::Other, msg).into());
            }
        }
        Ok((available_port, pids))
    } else {
        Err(io::Error::new(io::ErrorKind::Other, "Empty refresh_token").into())
    }
}

pub(crate) async fn handle_error(err: io::Error) -> impl IntoResponse {
    match err.kind() {
        io::ErrorKind::NotFound => (StatusCode::NOT_FOUND, err.to_string()),
        io::ErrorKind::PermissionDenied => (StatusCode::FORBIDDEN, err.to_string()),
        _ => (
            StatusCode::INTERNAL_SERVER_ERROR,
            "Something went wrong ..\n".to_string(),
        ),
    }
}

pub(crate) async fn device_poweroff() -> anyhow::Result<()> {
    tokio::process::Command::new("sudo")
        .arg("systemctl")
        .arg("poweroff")
        .spawn()?;
    Ok(())
}

pub(crate) async fn device_reboot() -> anyhow::Result<()> {
    tokio::process::Command::new("sudo")
        .arg("systemctl")
        .arg("reboot")
        .spawn()?;
    Ok(())
}

pub(crate) async fn service_restart() -> anyhow::Result<()> {
    tokio::process::Command::new("sudo")
        .arg("systemctl")
        .arg("restart")
        .arg("dlna.service")
        .spawn()?;
    Ok(())
}

fn save_state_internal<P>(p: P, states: &[data::Client]) -> anyhow::Result<()>
where
    P: AsRef<path::Path>,
{
    std::fs::write(p, serde_json::to_string(states)?)?;
    Ok(())
}

pub(crate) fn save_state<P>(p: P, states: &[data::Client])
where
    P: AsRef<path::Path>,
{
    match save_state_internal(p, states) {
        Ok(()) => tracing::info!(?states, "Client state saved:"),
        Err(error) => {
            tracing::error!(?error, ?states, "Could not save client states:");
        }
    }
}

pub(crate) async fn health_check(
    model: sync::Arc<tokio::sync::Mutex<data::Model>>,
    wait: time::Duration,
    interval: time::Duration,
) -> anyhow::Result<()> {
    tokio::time::sleep(wait).await;

    loop {
        let mut model = model.lock().await;
        let prev_mounted_clients = model.mounted_clients.clone();
        let mut all_are_healthy = true;

        for (nick, mounted_client) in &prev_mounted_clients {
            if !helpers::is_healthy(&model, &nick) {
                all_are_healthy = false;

                tracing::warn!(
                    "health_check: detected failed mount for nick '{}', attempting to kill and remount",
                    nick,
                );
                if !helpers::kill_batch_pids(&mounted_client.pids) {
                    tracing::warn!(
                        "Could not kill one or more processes of {:?}, ignoring",
                        &mounted_client.pids
                    );
                }

                match mount_drive(
                    helpers::get_refresh_token_for(&nick, &model.states),
                    helpers::get_mount_point_for(&nick, &model.cfg),
                    model.cfg.tmp_dir.clone(),
                ) {
                    Ok((port, pids)) => {
                        tracing::info!(
                            "health_check: attempted remount for nick '{}' (not necessarily successful)",
                            nick
                        );
                        model
                            .mounted_clients
                            .insert(nick.clone(), data::MountInfo::new(port, pids));
                    }
                    Err(e) => tracing::error!(
                        "health_check: remounting for nick '{}' failed: {}",
                        nick,
                        e,
                    ),
                }
            }
        }

        if all_are_healthy {
            std::fs::write(
                model.cfg.mount_dir.join(".last_checked"),
                // time::SystemTime::now().duration_since(time::UNIX_EPOCH)?.as_millis().to_string(),
                "",
            )?;
        }

        // Release the lock before sleeping or other requests during sleeping would get blocked
        drop(model);

        tokio::time::sleep(interval).await;
    }
}
