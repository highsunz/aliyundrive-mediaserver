use clap::Parser;
use serde::{Deserialize, Serialize};
use std::{collections, io, path};

#[derive(Parser, Serialize, Deserialize, Clone, Debug)]
#[command(name = "aliyundrive-mediaserver-backend", about, version, author)]
pub(crate) struct Opt {
    #[arg(short, long)]
    pub web_root: path::PathBuf,

    #[arg(short, long)]
    pub port: u16,

    #[arg(short, long)]
    pub mount_dir: path::PathBuf,

    // RClone configs will be written here, this directory is considered ephemeral.
    #[arg(short, long)]
    pub tmp_dir: path::PathBuf,

    #[arg(short, long)]
    pub state_file: path::PathBuf,

    #[arg(short, long)]
    pub localhost: bool,

    /// Duration in milliseconds to wait before starting healthchecks for mounted clients, default
    /// is 2000ms (2s).
    #[arg(long, default_value_t = 7000)]
    pub check_wait: u64,

    /// Interval in milliseconds for healthchecking mounted clients, default is 800ms (0.8s)
    #[arg(long, default_value_t = 5000)]
    pub check_interval: u64,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub(crate) struct Model {
    pub mounted_clients: collections::HashMap<String, MountInfo>,
    pub states: Vec<Client>,
    pub cfg: Opt,
}

impl Model {
    pub fn new_with_cfg(cfg: Opt) -> Self {
        Self {
            mounted_clients: collections::HashMap::new(),
            states: Vec::new(),
            cfg,
        }
    }
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub(crate) struct MountInfo {
    pub port: u16,
    pub pids: Vec<u32>,
}

impl MountInfo {
    pub fn new(port: u16, pids: Vec<u32>) -> Self {
        Self { port, pids }
    }
}

#[derive(Serialize, Deserialize, Debug)]
pub struct QrData {
    #[serde(rename = "qrCodeUrl")]
    pub qr_code_url: String,
    pub sid: String,
}

#[derive(Deserialize)]
pub struct LoginQueryPayload {
    pub sid: String,
    pub nick: String,
}

#[derive(Serialize, Deserialize, Clone, PartialEq)]
pub struct RefreshToken(String);

impl std::fmt::Debug for RefreshToken {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "<redacted>")
    }
}
impl std::fmt::Display for RefreshToken {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.0)
    }
}

impl From<String> for RefreshToken {
    fn from(value: String) -> Self {
        Self(value)
    }
}

impl RefreshToken {
    pub fn is_valid(bytes: &[u8]) -> bool {
        !bytes.is_empty()
    }

    pub fn from_bytes(bytes: &[u8]) -> anyhow::Result<Self> {
        if Self::is_valid(bytes) {
            Ok(Self(
                bytes
                    .iter()
                    .map(|b| *b as char)
                    .collect::<String>()
                    .trim()
                    .to_string(),
            ))
        } else {
            Err(io::Error::new(
                io::ErrorKind::Other,
                format!(
                    "Could not convert {} bytes into a RefreshToken",
                    bytes.len()
                ),
            )
            .into())
        }
    }

    pub(crate) fn token(self) -> String {
        self.0
    }
}

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq)]
pub struct Client {
    pub refresh_token: Option<RefreshToken>,
    pub info: ClientPublic,
}

impl Client {
    pub fn new(nick: String, refresh_token: Option<RefreshToken>, mounted: bool) -> Self {
        Client {
            refresh_token,
            info: ClientPublic::new(nick, mounted),
        }
    }

    pub fn with_refresh_token(&self, refresh_token: RefreshToken) -> Self {
        Client {
            refresh_token: Some(refresh_token),
            info: self.info.clone(),
        }
    }
}

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq)]
pub struct ClientPublic {
    pub nick: String,
    pub mounted: bool,
}

impl ClientPublic {
    pub fn new<S>(nick: S, mounted: bool) -> Self
    where
        S: AsRef<str>,
    {
        ClientPublic {
            nick: nick.as_ref().to_string(),
            mounted,
        }
    }
}

#[derive(Deserialize)]
pub enum DeviceControlAction {
    #[serde(rename = "reboot")]
    Reboot,
    #[serde(rename = "poweroff")]
    PowerOff,
}

#[derive(Deserialize)]
pub struct DeviceControlPayload {
    pub action: DeviceControlAction,
}

#[derive(Deserialize)]
pub enum ServiceControlAction {
    #[serde(rename = "restart")]
    Restart,
}

#[derive(Deserialize)]
pub struct ServiceControlPayload {
    pub action: ServiceControlAction,
}
