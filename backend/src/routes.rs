use axum::{extract::Path, http::StatusCode, response::IntoResponse, Json};

use crate::data;
use crate::utils;

// use crate::devel::{get_qr_data_internal, query_login_status_internal};
use crate::utils::{get_qr_data_internal, query_login_status_internal};

pub(crate) async fn get_qr_data(
    model: std::sync::Arc<tokio::sync::Mutex<data::Model>>,
    nick: String,
) -> impl IntoResponse {
    if utils::helpers::check_nick_availability(&model.lock().await.states, &nick) {
        match get_qr_data_internal().await {
            Ok(qr_data) => match qr_data {
                x @ Some(_) => (StatusCode::OK, Json(x)),
                None => (StatusCode::INTERNAL_SERVER_ERROR, Json(None)),
            },
            Err(_) => (StatusCode::INTERNAL_SERVER_ERROR, Json(None)),
        }
    } else {
        (StatusCode::BAD_REQUEST, Json(None))
    }
}

pub(crate) async fn login_query_status(
    model: std::sync::Arc<tokio::sync::Mutex<data::Model>>,
    Json(login_query_payload): Json<data::LoginQueryPayload>,
) -> impl IntoResponse {
    let data::LoginQueryPayload { sid, nick } = login_query_payload;
    match query_login_status_internal(sid).await {
        Ok(refresh_token_opt) => match refresh_token_opt {
            Some(refresh_token_str) => {
                let mut model = model.lock().await;
                let mdir = model.cfg.mount_dir.join(&nick);

                model.states = model
                    .states
                    .iter()
                    .map(|client| {
                        if client.info.nick == nick {
                            client.with_refresh_token(refresh_token_str.clone().into())
                        } else {
                            client.clone()
                        }
                    })
                    .collect();

                match utils::mount_drive(
                    Some(refresh_token_str.clone().into()),
                    mdir,
                    model.cfg.tmp_dir.clone(),
                ) {
                    Ok((port, pids)) => {
                        model
                            .mounted_clients
                            .insert(nick.clone(), data::MountInfo::new(port, pids));
                        model.states.push(data::Client::new(
                            nick,
                            Some(refresh_token_str.into()),
                            false,
                        ));
                        StatusCode::OK
                    }
                    Err(_) => StatusCode::INTERNAL_SERVER_ERROR,
                }
            }
            None => StatusCode::NOT_FOUND,
        },
        Err(_) => StatusCode::INTERNAL_SERVER_ERROR,
    }
}

pub(crate) async fn mount_client(
    model: std::sync::Arc<tokio::sync::Mutex<data::Model>>,
    nick: String,
) -> impl IntoResponse {
    let mut model = model.lock().await;
    let states = model.states.clone(); // makes the borrow checker happy
    let mdir = model.cfg.mount_dir.join(&nick);
    if let Some(client) = states.iter().find(|client| client.info.nick == nick) {
        if let Some(mounted_client) = model.mounted_clients.get(&nick) {
            tracing::warn!(
                "Got mount request for client '{}' which should already be mounted",
                &nick
            );
            if utils::helpers::is_healthy(&model, &nick) {
                // The request comes from an ill-working client
                let msg = format!(
                    "Client '{}' is already mounted, ignoring mounting request",
                    nick,
                );
                tracing::warn!("{}", msg);
                return (StatusCode::ALREADY_REPORTED, msg);
            } else {
                tracing::warn!(
                    "Client '{}' is not mounted at the moment, this could be caused by hanging mounting process, or the filesystem is unmounted outside of the control panel",
                    nick,
                );
                tracing::warn!("Trying to remount client '{}' anyway ...", nick,);

                if utils::helpers::kill_batch_pids(&mounted_client.pids) {
                    tracing::info!("Successfully killed previous mounting processes");
                } else {
                    tracing::warn!("Did not kill previous mounting process (could not kill one or more pids of {:?}), ignoring", &mounted_client.pids);
                }
                model.mounted_clients.remove(&nick);
            }
        }

        match utils::mount_drive(
            client.refresh_token.clone(),
            mdir,
            model.cfg.tmp_dir.clone(),
        ) {
            Ok((port, pids)) => {
                model
                    .mounted_clients
                    .insert(nick.clone(), data::MountInfo::new(port, pids));
                let msg = format!("Client '{}': successfully mounted", nick);
                tracing::info!(msg);
                (StatusCode::CREATED, msg)
            }
            Err(e) => {
                let msg = format!("Could not mount drive for client '{}': {}", nick, e);
                tracing::error!(msg);
                (StatusCode::INTERNAL_SERVER_ERROR, msg)
            }
        }
    } else {
        let error = format!("Could not find requested client with nick '{}'", nick);
        tracing::error!(error);
        (StatusCode::BAD_REQUEST, error)
    }
}

pub(crate) async fn unmount_client(
    model: std::sync::Arc<tokio::sync::Mutex<data::Model>>,
    Path(nick): Path<String>,
) -> impl IntoResponse {
    let mut model = model.lock().await;
    match model.mounted_clients.get(&nick) {
        Some(mounted_client) => {
            if utils::helpers::kill_batch_pids(&mounted_client.pids) {
                model.mounted_clients.remove(&nick);
                let msg = format!("Client '{}': successfully unmounted", nick);
                tracing::info!(msg);
                (StatusCode::OK, msg)
            } else {
                let msg = format!(
                    "Error unmounting client '{}': could not kill one or more pids of {:?}",
                    nick, &mounted_client.pids
                );
                tracing::error!(msg);
                (StatusCode::NOT_FOUND, msg)
            }
        }
        None => {
            let msg = format!(
                "Could not find a client named '{}' that has been mounted",
                nick
            );
            tracing::error!(msg);
            (StatusCode::NOT_FOUND, msg)
        }
    }
}

pub(crate) async fn remove_token(
    model: std::sync::Arc<tokio::sync::Mutex<data::Model>>,
    Path(nick): Path<String>,
) -> impl IntoResponse {
    let mut model = model.lock().await;
    if utils::helpers::is_healthy(&model, &nick) {
        StatusCode::BAD_REQUEST
    } else if model.states.iter().all(|client| client.info.nick != nick) {
        StatusCode::ALREADY_REPORTED
    } else {
        model.states.retain(|client| client.info.nick != nick);
        utils::save_state(&model.cfg.state_file, &model.states);
        StatusCode::OK
    }
}

pub(crate) async fn get_all_clients(
    model: std::sync::Arc<tokio::sync::Mutex<data::Model>>,
) -> impl IntoResponse {
    let mut model = model.lock().await;

    let clients: Vec<data::Client> = model
        .states
        .iter()
        .map(|client| {
            let nick = &client.info.nick;
            data::Client::new(
                nick.clone(),
                client.refresh_token.clone(),
                utils::helpers::is_healthy(&model, &nick),
            )
        })
        .collect();
    if model.states.len() == clients.len()
        && model.states.iter().all(|x| clients.iter().any(|y| x == y))
    {
        tracing::debug!(?model.states, "Logged-in clients unchanged");
    } else {
        model.states = clients.clone();
        tracing::info!(
            "Logged-in clients: {}, mounted clients: {}",
            clients.len(),
            clients.iter().filter(|c| c.info.mounted).count(),
        );
        utils::save_state(&model.cfg.state_file, &model.states);
    }

    (
        StatusCode::OK,
        Json(
            clients
                .into_iter()
                .map(|client| client.info)
                .collect::<Vec<_>>(),
        ),
    )
}

pub(crate) async fn device_ctl(
    Json(payload): Json<data::DeviceControlPayload>,
) -> impl IntoResponse {
    match payload.action {
        data::DeviceControlAction::Reboot => {
            tracing::warn!("Rebooting device");
            match utils::device_reboot().await {
                Ok(()) => (StatusCode::OK, "".to_string()),
                Err(error) => {
                    tracing::error!(?error, "Could not reboot device");
                    (StatusCode::INTERNAL_SERVER_ERROR, error.to_string())
                }
            }
        }
        data::DeviceControlAction::PowerOff => {
            tracing::warn!("Shutting down device");
            match utils::device_poweroff().await {
                Ok(()) => (StatusCode::OK, "".to_string()),
                Err(error) => {
                    tracing::error!(?error, "Could not shutdown device");
                    (StatusCode::INTERNAL_SERVER_ERROR, error.to_string())
                }
            }
        }
    }
}

pub(crate) async fn service_ctl(
    Json(payload): Json<data::ServiceControlPayload>,
) -> impl IntoResponse {
    match payload.action {
        data::ServiceControlAction::Restart => {
            tracing::warn!("Restarting service");
            match utils::service_restart().await {
                Ok(()) => (StatusCode::OK, "".to_string()),
                Err(error) => {
                    tracing::error!(?error, "Could not restart service");
                    (StatusCode::INTERNAL_SERVER_ERROR, error.to_string())
                }
            }
        }
    }
}
