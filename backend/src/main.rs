use axum::{
    http::header::{HeaderMap, HeaderValue, CACHE_CONTROL},
    routing::{self, get_service},
    Router,
};
use clap::Parser;
use std::net::SocketAddr;

use tower_default_headers::DefaultHeadersLayer;
use tower_http::services::ServeDir;

mod data;
mod devel;
mod routes;
mod utils;

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    setup();

    let opt = data::Opt::parse();

    utils::helpers::check_and_create_dir(&opt.mount_dir, "Mounting root")?;
    utils::helpers::check_and_create_dir(&opt.tmp_dir, "Directory for ephemeral data")?;

    let model = if let Ok(prev_states) = utils::load_state_file(&opt.state_file) {
        tracing::info!("Restoring previous state");
        let prev_mounted_clients = prev_states
            .iter()
            .filter_map(|client| {
                if client.info.mounted {
                    if let Ok((port, pids)) = utils::mount_drive(
                        client.refresh_token.clone(),
                        opt.mount_dir.join(&client.info.nick),
                        opt.tmp_dir.clone(),
                    ) {
                        Some((client.info.nick.clone(), data::MountInfo::new(port, pids)))
                    } else {
                        None
                    }
                } else {
                    None
                }
            })
            .collect();
        data::Model {
            mounted_clients: prev_mounted_clients,
            states: prev_states,
            cfg: opt.clone(),
        }
    } else {
        data::Model::new_with_cfg(opt.clone())
    };
    tracing::info!(?model, "Initialized");

    let model = std::sync::Arc::new(tokio::sync::Mutex::new(model));

    // serve web page and stuff
    let serve_dir = get_service(ServeDir::new(&opt.web_root)).handle_error(utils::handle_error);
    let app = Router::new().nest_service("/", serve_dir);

    // query states
    let app = app
        .route(
            "/api/v1/qrdata",
            routing::post({
                let model = model.clone();
                move |nick| routes::get_qr_data(model, nick)
            }),
        )
        .route(
            "/api/v1/login/allclients",
            routing::get({
                let model = model.clone();
                move || routes::get_all_clients(model)
            }),
        )
        .route(
            "/api/v1/login/query",
            routing::post({
                let model = model.clone();
                move |payload| routes::login_query_status(model, payload)
            }),
        );

    // mount client
    let app = app.route(
        "/api/v1/drive/mount",
        routing::post({
            let model = model.clone();
            move |nick| routes::mount_client(model, nick)
        }),
    );

    // unmount and delete client
    let app = app
        .route(
            "/api/v1/drive/umount/:nick",
            routing::delete({
                let model = model.clone();
                move |nick_path| routes::unmount_client(model, nick_path)
            }),
        )
        .route(
            "/api/v1/drive/rmtoken/:nick",
            routing::delete({
                let model = model.clone();
                move |path| routes::remove_token(model, path)
            }),
        );

    // device/service controlling
    let app = app
        .route("/api/v1/ctrl/device", routing::post(routes::device_ctl))
        .route("/api/v1/ctrl/upnp", routing::post(routes::service_ctl));

    // default headers
    let mut default_headers = HeaderMap::new();
    default_headers.insert(
        CACHE_CONTROL,
        HeaderValue::from_static("max-age=0, private, must-revalidate"),
    );
    let app = app.layer(DefaultHeadersLayer::new(default_headers));

    let addr = SocketAddr::from((
        if opt.localhost {
            [127, 0, 0, 1]
        } else {
            [0, 0, 0, 0]
        },
        opt.port,
    ));
    tracing::info!("Serving at {:?}", opt.web_root);
    tracing::info!("Listening on {}", addr);

    let (server_result, health_checker_result) = tokio::join!(
        axum::Server::bind(&addr).serve(app.into_make_service()),
        utils::health_check(
            model.clone(),
            tokio::time::Duration::from_millis(opt.check_wait),
            tokio::time::Duration::from_millis(opt.check_interval),
        )
    );

    server_result?;
    health_checker_result?;

    Ok(())
}

fn setup() {
    if std::env::var("RUST_LOG").is_err() {
        std::env::set_var("RUST_LOG", "info");
    }
    tracing_subscriber::fmt::init();
}
