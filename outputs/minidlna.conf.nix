{ name
, mediaRoot
, stateDirectory
, logsDirectory
, rootContainer
}: ''
friendly_name=${name}
media_dir=${mediaRoot}
db_dir=${stateDirectory}
log_dir=${logsDirectory}
root_container=${rootContainer}
inotify=no
''
