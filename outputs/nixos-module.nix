{ config, lib, pkgs, ... }: let
  cfg = config.services.aliyundrive-mediaserver;
in with lib; {
  options.services.aliyundrive-mediaserver = {
    enable = mkEnableOption "Enable service that probes media from aliyundrive instances";
    package = {
      backend = mkOption {
        type = types.package;
      };
      frontend = mkOption {
        type = types.package;
      };
      aliyundrive-webdav = mkOption {
        type = types.package;
      };
      rclone = mkOption {
        type = types.package;
        default = pkgs.rclone;
      };
    };
    port = {
      aliyundrive-mediaserver = mkOption {
        type = with types; oneOf [ int str ];
        description = "Port for aliyundrive-mediaserver to listen on";
      };
      file-explorer = mkOption {
        type = with types; oneOf [ int str ];
        description = "Port for the web file-explorer to listen on";
      };
    };
    explorerPathPrefix = mkOption {
      type = types.str;
      description = "Passed as the `--path-prefix` option to dufs";
      default = "explore";
    };
    serverName = mkOption {
      type = types.str;
      default = "mediaserver";
    };
    notifyInterval = mkOption {
      type = types.int;
      default = 5;
    };
  };

  config = mkIf cfg.enable {
    assertions = [{
      assertion = !(builtins.elem "/" (lib.stringToCharacters cfg.explorerPathPrefix));
      message = ''
        services.aliyundrive-mediaserver.explorerPathPrefix should not contain literal slashes
      '';
    }];

    users = {
      users.aliyundrive-mediaserver = {
        isSystemUser = true;
        group = config.users.groups.aliyundrive-mediaserver.name;
      };
      groups.aliyundrive-mediaserver = {};
    };

    security.sudo.extraRules = let
      applyNoPasswd = cmd: { command = cmd; options = [ "NOPASSWD" ]; };
      cmds = map
        applyNoPasswd
          (map
            (cmd: "${pkgs.systemd}/bin/systemctl ${cmd}")
            [
              "reboot"
              "poweroff"
              "restart dlna.service"
            ]
          );
    in [{
      users = [ config.users.users.aliyundrive-mediaserver.name ];
      commands = cmds;
    }];

    environment.systemPackages = with cfg.package; [
      backend
      frontend
      aliyundrive-webdav
      rclone
    ];

    programs.fuse.userAllowOther = true;

    systemd.targets.mediaservices = {
      requires = [
        "network-online.target"

        # WARN: adding these to Requires will cause a result where restarting these services also
        # reloads the whole mediaservices.target
        #
        # "aliyundrive-mediaserver.service"
        # "dlna.service"
        # "file-explorer.service"
      ];
      after = [
        "network-online.target"

        "aliyundrive-mediaserver.service"
        "dlna.service"
        "file-explorer.service"
      ];
      wantedBy = [ "multi-user.target" ];
    };

    systemd.services.aliyundrive-mediaserver = rec {
      after = [ "network-online.target" "network.target" ];
      wantedBy = [ "mediaservices.target" ];
      partOf = [ "mediaservices.target" ];
      path = [
        cfg.package.backend
        cfg.package.aliyundrive-webdav
        cfg.package.rclone
        pkgs.systemd
        "/run/wrappers"
      ];
      script = ''
        aliyundrive-mediaserver-backend \
          --web-root=${cfg.package.frontend}/share/webapps/aliyundrive-mediaserver \
          --localhost \
          --port=${toString cfg.port.aliyundrive-mediaserver} \
          --state-file=/var/lib/${serviceConfig.StateDirectory}/states.json \
          --mount-dir=/run/${serviceConfig.RuntimeDirectory}/mount \
          --tmp-dir=/run/${serviceConfig.RuntimeDirectory}/cfg
      '';
      # avoid dangling fuse fs left by dead rclone
      preStart = ''
        rm -vf /run/${serviceConfig.RuntimeDirectory}/mount/.last_checked
        for d in /run/${serviceConfig.RuntimeDirectory}/mount/*; do
          if [[ -d "$d" ]]; then
            echo "Attempting to unmount directory '$d'"
            umount "$d" || true
          fi
        done
      '';
      postStop = preStart + ''
        rm -vf /run/${serviceConfig.RuntimeDirectory}/mount/.last_checked
      '';
      serviceConfig = {
        User = config.users.users.aliyundrive-mediaserver.name;
        Group = config.users.groups.aliyundrive-mediaserver.name;
        StateDirectory = "aliyundrive-mediaserver";
        StateDirectoryMode = "0700";
        RuntimeDirectory = "aliyundrive-mediaserver";
        RuntimeDirectoryMode = "0700";
        Restart = "always";
        RestartSec = 5;
      };
    };

    systemd.services.dlna = let
      mediaRoot = "/run/${config.systemd.services.aliyundrive-mediaserver.serviceConfig.RuntimeDirectory}/mount";
    in rec {
      # NOTE: Must set this so that restarting `aliyundrive-mediaserver.service` will first stop
      # `dlna.service`.  This avoids some fuse filesystem is being read while also being requested
      # to unmounted, which causes dangling `fuse.rclone` filesystems that requires manual
      # intervention.
      requires = [ "aliyundrive-mediaserver.service" ];
      after = [ "aliyundrive-mediaserver.service" "network.target" "network-online.target" ];
      wantedBy = [ "mediaservices.target" ];
      partOf = [ "mediaservices.target" ];
      path = [ pkgs.minidlna ];
      preStart = ''
        ok_file=${mediaRoot}/.last_checked
        check_interval=1

        rm -vf "$ok_file"

        while [[ ! -e "$ok_file" ]]; do
          echo "waiting for '$ok_file' to become present ..."
          sleep "$check_interval"
        done

        echo "health check success, starting service"
      '';
      script = let
        dlnaConfig = pkgs.writeText "dlna.conf" (import ./minidlna.conf.nix {
            name = cfg.serverName;
            inherit mediaRoot;
            stateDirectory = "/var/lib/${serviceConfig.StateDirectory}";
            logsDirectory = "/var/lib/${serviceConfig.LogsDirectory}";
            rootContainer = "B";
          });
        flags = [
            "-f" dlnaConfig  # specify config file
            "-t" cfg.notifyInterval
            "-S"  # Use stdout for loging
            "-r"  # reload (only those newly modified)
            # "-R"  # reload (aggressively, i.e. recreate db)
            "-P" "/run/${serviceConfig.RuntimeDirectory}/pid"
          ];
      in ''
        minidlnad ${lib.escapeShellArgs flags}
      '';
      serviceConfig = {
        User = config.users.users.aliyundrive-mediaserver.name;
        Group = config.users.groups.aliyundrive-mediaserver.name;
        StateDirectory = "dlna";
        StateDirectoryMode = "0700";
        LogsDirectory = "dlna";
        LogsDirectoryMode = "0700";
        RuntimeDirectory = "dlna";
        RuntimeDirectoryMode = "0755";
        Restart = "always";
        RestartSec = 5;
      };
    };

    systemd.services.file-explorer = {
      after = [ "aliyundrive-mediaserver.service" "network.target" "network-online.target" ];
      wantedBy = [ "mediaservices.target" ];
      partOf = [ "mediaservices.target" ];
      path = [ pkgs.dufs ];
      script = ''
        dufs "/run/${config.systemd.services.aliyundrive-mediaserver.serviceConfig.RuntimeDirectory}/mount" \
          --port ${toString cfg.port.file-explorer} --bind=0.0.0.0 \
          --path-prefix ${cfg.explorerPathPrefix} \
          --log-format='req:$request | user:$remote_user | client:$remote_addr | code:$status | ua:$http_user_agent'
      '';
      serviceConfig = {
        User = config.users.users.aliyundrive-mediaserver.name;
        Group = config.users.groups.aliyundrive-mediaserver.name;
        Restart = "always";
        RestartSec = 5;
      };
    };
  };
}
