{
  description = "NixOS service that probes media from aliyundrive instances";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/release-24.05";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils, ... }: flake-utils.lib.eachSystem [ "aarch64-linux" "x86_64-linux" ] (system: let
    pkgs = import nixpkgs {
      inherit system;
      overlays = [
        self.overlays.default
      ];
    };
  in {
    packages = rec {
      default = aliyundrive-mediaserver;
      backend = aliyundrive-mediaserver-backend;
      frontend = aliyundrive-mediaserver-frontend;

      inherit (pkgs)
        aliyundrive-mediaserver
        aliyundrive-mediaserver-backend
        aliyundrive-mediaserver-frontend
        aliyundrive-webdav;
    };
    devShells = {
      frontend = pkgs.mkShell {
        name = "frontend";
        buildInputs = with pkgs.elmPackages; [
          elm
          elm-format
          elm-live  # elm-live -h 0 src/Main.elm --start-page=index.html -- --output=index.js
        ];
        shellHook = ''
          # If interactive
          [[ "$-" == *i* ]] && exec "$SHELL"
        '';
      };
      backend = pkgs.mkShell {
        name = "backend";
        buildInputs = with pkgs; [
          cargo
          cargo-edit
          clippy
          rustc
          rustfmt
          rust-analyzer
          aliyundrive-webdav
          rclone
        ];
        shellHook = ''
          # If interactive
          [[ "$-" == *i* ]] && exec "$SHELL"
        '';
      };
    };
  }) // {
    overlays = {
      default = final: prev: let
        version = "0.2.0";
        generated = import ./outputs/packages/_sources/generated.nix {
          inherit (final) fetchurl fetchgit fetchFromGitHub dockerTools;
        };
      in rec {
        aliyundrive-mediaserver = final.symlinkJoin {
          name = "aliyundrive-mediaserver-${version}";
          paths = [
            aliyundrive-mediaserver-backend
            aliyundrive-mediaserver-frontend
          ];
        };
        aliyundrive-mediaserver-backend = final.callPackage ./backend { inherit version; };
        # run elm2nix in ./frontend
        aliyundrive-mediaserver-frontend = final.callPackage ./frontend { inherit version; };

        aliyundrive-webdav = final.callPackage ./outputs/packages/aliyundrive-webdav.nix { source = generated.aliyundrive-webdav; };
      };
    };

    nixosModules.default = import ./outputs/nixos-module.nix;

    hydraJobs = self.packages;
  };
}
