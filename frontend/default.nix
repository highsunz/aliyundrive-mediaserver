{ version, stdenvNoCC
, elmPackages
}: stdenvNoCC.mkDerivation {
  pname = "aliyundrive-mediaserver-frontend";
  inherit version;
  src = ./.;
  buildInputs = [ elmPackages.elm ];
  buildPhase = elmPackages.fetchElmDeps {
    elmPackages = import ./elm-srcs.nix;
    elmVersion = "0.19.1";
    registryDat = ./registry.dat;
  } + ''
    elm make $src/src/Main.elm --output=index.js
  '';
  installPhase = ''
    install -Dm644 -t $out/share/webapps/aliyundrive-mediaserver \
      index.js $src/index.html
    install -Dm644 -t $out/share/webapps/aliyundrive-mediaserver/assets \
      $src/assets/*
  '';
}
