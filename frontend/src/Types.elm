module Types exposing (..)

import Http
import Time


type Msg
    = GetAndDisplayNewQrData
    | HideQr
    | GotNewQrData (Result Http.Error QrData)
    | QueryNewLoginStatus Time.Posix
    | QueryAllLoggedInClients Time.Posix
    | GotNewLoginStatus (Result Http.Error ())
    | SetNewLoginNick String
    | GotAllClients (Result Http.Error (List Client))
    | DriveMount String
    | DriveUnmount String
    | DriveRemoveToken String
    | SentMountCmd (Result Http.Error ())
    | SentUnmountAndRemoveTokenCmd (Result Http.Error ())
    | SentRemoveTokenCmd (Result Http.Error ())
    | RequestDeviceReboot
    | RequestDevicePowerOff
    | RequestUpnpServiceRestart
    | SentRequestDeviceRebootCmd (Result Http.Error ())
    | SentRequestDevicePowerOffCmd (Result Http.Error ())
    | SentRequestUpnpServiceRestartCmd (Result Http.Error ())
    | OpenUrlInNewTab String


type alias QrData =
    { qrCodeUrl: String
    , sid: String
    }


type QrGettingStatus
    = QrWaiting
    | QrSuccess
    | QrBadRequest -- Happens if nick is invalid (e.g. duplicated, value is ".", etc.)
    | QrFailure


type alias QrInfo =
    { qrGettingStatus : QrGettingStatus
    , qrData : Maybe QrData
    , display : Bool
    }


type LoginStatus
    = LoginWaiting
    | LoginSuccess
    | LoginIdle


type alias NewLoginInfo =
    { nick : String
    , qr : QrInfo
    , status : LoginStatus
    }


type alias Client =
    { nick : String
    , mounted : Bool
    }


type alias Model =
    { loggedInClients : List Client
    , newLoginInfo : NewLoginInfo
    }



-- model helpers


asNewLoginInfoIn : Model -> NewLoginInfo -> Model
asNewLoginInfoIn model newLoginInfo =
    { model | newLoginInfo = newLoginInfo }
