module Cmds exposing (..)

import Decoders exposing (..)
import Http
import Json.Encode as JE
import Types exposing (..)


queryNewLoginStatusWithNickCmd : NewLoginInfo -> Cmd Msg
queryNewLoginStatusWithNickCmd newLoginInfo =
    case newLoginInfo.qr.qrData of
        Just x ->
            Http.post
                { url = "/api/v1/login/query"
                , body =
                    JE.object
                        [ ( "sid", JE.string x.sid )
                        , ( "nick", JE.string newLoginInfo.nick )
                        ]
                        |> Http.jsonBody
                , expect = Http.expectWhatever GotNewLoginStatus
                }

        Nothing ->
            Cmd.none


queryAllLoggedInClientsCmd : Cmd Msg
queryAllLoggedInClientsCmd =
    Http.get
        { url = "/api/v1/login/allclients"
        , expect = Http.expectJson GotAllClients allClientsDecoder
        }


mountCmd : String -> Cmd Msg
mountCmd nick =
    Http.post
        { url = "/api/v1/drive/mount"
        , body = Http.stringBody "text/plain" nick
        , expect = Http.expectWhatever SentMountCmd
        }


unmountCmd : String -> Cmd Msg
unmountCmd nick =
    Http.request
        { method = "DELETE"
        , headers = []
        , url = "/api/v1/drive/umount/" ++ nick
        , body = Http.emptyBody
        , expect = Http.expectWhatever SentUnmountAndRemoveTokenCmd
        , timeout = Just 5
        , tracker = Nothing
        }


removeTokenCmd : String -> Cmd Msg
removeTokenCmd nick =
    Http.request
        { method = "DELETE"
        , headers = []
        , url = "/api/v1/drive/rmtoken/" ++ nick
        , body = Http.emptyBody
        , expect = Http.expectWhatever SentRemoveTokenCmd
        , timeout = Just 5
        , tracker = Nothing
        }


requestDeviceRebootCmd : Cmd Msg
requestDeviceRebootCmd =
    Http.post
        { url = "/api/v1/ctrl/device"
        , body = JE.object [ ( "action", JE.string "reboot" ) ] |> Http.jsonBody
        , expect = Http.expectWhatever SentRequestDeviceRebootCmd
        }


requestDevicePowerOffCmd : Cmd Msg
requestDevicePowerOffCmd =
    Http.post
        { url = "/api/v1/ctrl/device"
        , body = JE.object [ ( "action", JE.string "poweroff" ) ] |> Http.jsonBody
        , expect = Http.expectWhatever SentRequestDevicePowerOffCmd
        }


requestUpnpServiceRestartCmd : Cmd Msg
requestUpnpServiceRestartCmd =
    Http.post
        { url = "/api/v1/ctrl/upnp"
        , body = JE.object [ ( "action", JE.string "restart" ) ] |> Http.jsonBody
        , expect = Http.expectWhatever SentRequestUpnpServiceRestartCmd
        }


getNewQrDataCmd : String -> Cmd Msg
getNewQrDataCmd nick =
    case String.trim nick of
        "" ->
            Cmd.none

        nickTrimmed ->
            Http.post
                { url = "/api/v1/qrdata"
                , body = Http.stringBody "text/plain" nickTrimmed
                , expect = Http.expectJson GotNewQrData qrDataDecoder
                }
