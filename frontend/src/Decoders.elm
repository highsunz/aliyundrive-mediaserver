module Decoders exposing (..)

import Json.Decode as JD exposing (Decoder)
import Types exposing (..)


qrDataDecoder : Decoder QrData
qrDataDecoder =
    JD.map2 QrData
        (JD.field "qrCodeUrl" JD.string)
        (JD.field "sid" JD.string)


clientDecoder : Decoder Client
clientDecoder =
    JD.map2 Client
        (JD.field "nick" JD.string)
        (JD.field "mounted" JD.bool)


allClientsDecoder : Decoder (List Client)
allClientsDecoder =
    JD.list clientDecoder
