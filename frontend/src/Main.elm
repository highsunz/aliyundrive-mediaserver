port module Main exposing (main)

import Browser exposing (Document)
import Cmds exposing (..)
import Configs
import Decoders exposing (..)
import Element exposing (Element, alignLeft, alignRight, alignTop, centerX, centerY, column, el, fill, fillPortion, height, html, htmlAttribute, image, padding, paddingEach, px, row, spacing, text, width)
import Element.Background
import Element.Border
import Element.Font as EF
import Element.Input
import Html.Attributes as HA
import Http
import Platform.Cmd
import Platform.Sub
import QRCode
import Time
import Types exposing (..)


port openUrlInNewTab : String -> Cmd msg


main : Program () Model Msg
main =
    Browser.document
        { init = init
        , update = update
        , view = view
        , subscriptions = subscriptions
        }


init : flags -> ( Model, Cmd Msg )
init _ =
    ( { loggedInClients = []
      , newLoginInfo =
            { nick = ""
            , qr =
                QrInfo
                    -- qrGettingStatus
                    QrWaiting
                    -- qrData
                    Nothing
                    -- display
                    False
            , status = LoginIdle
            }
      }
    , Platform.Cmd.batch
        [ queryAllLoggedInClientsCmd
        ]
    )


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        GotNewQrData result ->
            ( NewLoginInfo
                model.newLoginInfo.nick
                (case result of
                    Ok x ->
                        QrInfo
                            -- qrGettingStatus
                            QrSuccess
                            -- qrData
                            (Just x)
                            -- display
                            True

                    Err e ->
                        QrInfo
                            -- qrGettingStatus
                            (if e == Http.BadStatus 400 then
                                QrBadRequest

                             else
                                QrFailure
                            )
                            -- qrData
                            Nothing
                            -- display
                            True
                )
                model.newLoginInfo.status
                |> asNewLoginInfoIn model
            , Cmd.none
            )

        GetAndDisplayNewQrData ->
            ( NewLoginInfo
                model.newLoginInfo.nick
                (QrInfo
                    -- qrGettingStatus
                    QrWaiting
                    -- qrData
                    Nothing
                    -- display
                    True
                )
                LoginWaiting
                |> asNewLoginInfoIn model
            , getNewQrDataCmd model.newLoginInfo.nick
            )

        HideQr ->
            ( NewLoginInfo
                model.newLoginInfo.nick
                (QrInfo
                    -- qrGettingStatus
                    QrWaiting
                    -- qrData
                    Nothing
                    -- display
                    False
                )
                LoginIdle
                |> asNewLoginInfoIn model
            , Cmd.none
            )

        GotNewLoginStatus result ->
            ( case result of
                Err _ ->
                    NewLoginInfo
                        model.newLoginInfo.nick
                        model.newLoginInfo.qr
                        LoginWaiting
                        |> asNewLoginInfoIn model

                _ ->
                    NewLoginInfo
                        ""
                        (QrInfo
                            -- qrGettingStatus
                            QrWaiting
                            -- qrData
                            Nothing
                            -- display
                            False
                        )
                        LoginSuccess
                        |> asNewLoginInfoIn model
            , Cmd.none
            )

        QueryNewLoginStatus _ ->
            ( model
            , if model.newLoginInfo.qr.display then
                -- queryNewLoginStatusCmd model.newLoginInfo.qr
                queryNewLoginStatusWithNickCmd model.newLoginInfo

              else
                Cmd.none
            )

        QueryAllLoggedInClients _ ->
            ( model
            , queryAllLoggedInClientsCmd
            )

        SetNewLoginNick newLoginNick ->
            ( NewLoginInfo
                newLoginNick
                (QrInfo
                    model.newLoginInfo.qr.qrGettingStatus
                    model.newLoginInfo.qr.qrData
                    False
                )
                model.newLoginInfo.status
                |> asNewLoginInfoIn model
            , Cmd.none
            )

        GotAllClients result ->
            case result of
                Ok clients ->
                    ( { model | loggedInClients = clients }
                    , Cmd.none
                    )

                _ ->
                    ( model, Cmd.none )

        DriveMount nick ->
            ( model
            , mountCmd nick
            )

        DriveUnmount nick ->
            ( model, unmountCmd nick )

        DriveRemoveToken nick ->
            ( model, removeTokenCmd nick )

        SentMountCmd _ ->
            -- Do nothing, update of logged-in clients is queried per second
            ( model, Cmd.none )

        SentUnmountAndRemoveTokenCmd _ ->
            -- Do nothing, update of logged-in clients is queried per second
            ( model, Cmd.none )

        SentRemoveTokenCmd _ ->
            -- Do nothing, update of logged-in clients is queried per second
            ( model, Cmd.none )

        RequestDeviceReboot ->
            ( model, requestDeviceRebootCmd )

        RequestDevicePowerOff ->
            ( model, requestDevicePowerOffCmd )

        RequestUpnpServiceRestart ->
            ( model
            , requestUpnpServiceRestartCmd
            )

        SentRequestDeviceRebootCmd _ ->
            ( model, Cmd.none )

        SentRequestDevicePowerOffCmd _ ->
            ( model, Cmd.none )

        SentRequestUpnpServiceRestartCmd _ ->
            ( model
            , Cmd.none
            )

        OpenUrlInNewTab url ->
            ( model, openUrlInNewTab url )


view : Model -> Document Msg
view model =
    { title = Configs.siteTitle
    , body =
        [ Element.layout
            [-- rgb255 0x7D 0x9D 0x9C |> Element.Background.color
            ]
            (column
                [ centerX
                , centerY
                , spacing 32
                ]
                [ viewTitle
                , viewLoggedInClients model.loggedInClients
                , viewNewLoginPrompt model.newLoginInfo
                , viewControlPanel
                ]
            )
        ]
    }


subscriptions : Model -> Sub Msg
subscriptions model =
    List.concat
        [ if model.newLoginInfo.qr.display then
            [ Time.every 3001 QueryNewLoginStatus ]

          else
            []
        , [ Time.every 503 QueryAllLoggedInClients
          ]
        ]
        |> Platform.Sub.batch


viewTitle : Element Msg
viewTitle =
    text Configs.siteTitle
        |> el
            [ alignTop
            , centerX
            , EF.size 39
            , EF.bold
            ]


viewLoggedInClients : List Client -> Element Msg
viewLoggedInClients clients =
    column
        [ width fill
        , spacing 5
        ]
        (List.map viewClient clients)


viewClient : Client -> Element Msg
viewClient client =
    row
        [ width fill
        , paddingEach
            { top = 6
            , bottom = 8
            , left = 8
            , right = 20
            }
        , spacing 16
        , Element.Border.glow Configs.uiColors.gray 2
        , Element.Border.rounded 8
        , Configs.dimmedColorForStatus client.mounted |> Element.Background.color
        ]
        ([ viewClientMountStatus client.mounted
            |> el [ alignLeft ]
         , client.nick
            |> text
            |> el [ EF.underline, EF.bold ]
         ]
            ++ (viewClientActionButton client
                    |> List.map
                        (el
                            [ alignRight
                            , Element.Border.rounded 999
                            , Element.Border.shadow
                                { offset = ( 1, 1 )
                                , size = 2
                                , blur = 2
                                , color = Configs.uiColors.gray
                                }
                            ]
                        )
               )
        )


viewClientMountStatus : Bool -> Element Msg
viewClientMountStatus mounted =
    row
        [ width fill
        , paddingEach
            { top = 6
            , bottom = 6
            , left = 8
            , right = 8
            }
        , spacing 8
        , Element.Border.rounded 8
        , Element.Border.glow (Configs.colorForStatus mounted) 6
        , Configs.colorForStatus mounted |> Element.Background.color
        ]
        [ image
            [ px 24 |> width
            , px 24 |> height
            , alignRight
            ]
            (if mounted then
                { src = "/assets/checkmark.svg"
                , description = "A checkmark icon, indicating success"
                }

             else
                { src = "/assets/eye-closed.svg"
                , description = "An icon of a computer, with a cross mark in front of it"
                }
            )
        ]


viewClientActionButton : Client -> List (Element Msg)
viewClientActionButton client =
    Element.Input.button
        [ HA.style "box-shadow" "none" |> htmlAttribute
        , Element.Border.rounded 999
        , padding 6
        , Configs.colorForStatus False |> Element.Background.color
        ]
        { onPress =
            Just
                (if client.mounted then
                    DriveUnmount client.nick

                 else
                    DriveRemoveToken client.nick
                )
        , label =
            image
                [ px 24 |> width
                , px 24 |> height
                ]
                (if client.mounted then
                    { src = "/assets/eye-slash.svg"
                    , description = "An eye with a slash on top"
                    }

                 else
                    { src = "/assets/delete.svg"
                    , description = "An icon of a trash bin"
                    }
                )
        }
        :: (if client.mounted then
                [ Element.Input.button
                    [ HA.style "box-shadow" "none" |> htmlAttribute
                    , Element.Border.rounded 999
                    , padding 6
                    , Configs.colorForStatus True |> Element.Background.color
                    ]
                    { onPress =
                        Just
                            ("/explore/" ++ client.nick |> OpenUrlInNewTab)
                    , label =
                        image
                            [ px 24 |> width
                            , px 24 |> height
                            ]
                            { src = "/assets/open-in-new-tab.svg"
                            , description = "An arrow pointing through the upper-right corner of a square"
                            }
                    }
                ]

            else
                [ Element.Input.button
                    [ HA.style "box-shadow" "none" |> htmlAttribute
                    , Element.Border.rounded 999
                    , padding 6
                    , Configs.colorForStatus (not client.mounted) |> Element.Background.color
                    ]
                    { onPress = Just (DriveMount client.nick)
                    , label =
                        image
                            [ px 24 |> width
                            , px 24 |> height
                            ]
                            { src = "/assets/add.svg"
                            , description = "An add sign"
                            }
                    }
                ]
           )


viewNewLoginPrompt : NewLoginInfo -> Element Msg
viewNewLoginPrompt newLoginInfo =
    column
        [ centerX
        , centerY
        , spacing 26
        ]
        (viewNickInput newLoginInfo
            :: (if newLoginInfo.qr.display then
                    if String.isEmpty newLoginInfo.nick then
                        []

                    else
                        [ viewQrButton newLoginInfo.qr ]

                else
                    []
               )
        )


viewQr : QrInfo -> Element Msg
viewQr qrInfo =
    case qrInfo.qrGettingStatus of
        QrWaiting ->
            text "获取登录二维码中……"
                |> el
                    [ centerX
                    , centerY
                    ]

        QrSuccess ->
            case qrInfo.qrData of
                Just qrData ->
                    image
                        [ width fill
                        , height fill
                        ]
                        { src = qrData.qrCodeUrl
                        , description = "QR code for logging in aliyun drive"
                        }

                Nothing ->
                    text "Unreachable"

        QrBadRequest ->
            text "新文件夹名与已有的重复"
                |> el
                    [ centerX
                    , centerY
                    ]

        QrFailure ->
            text "登录二维码获取失败"
                |> el
                    [ centerX, centerY ]


viewQrButton : QrInfo -> Element Msg
viewQrButton qrInfo =
    Element.Input.button
        [ HA.style "box-shadow" "none" |> htmlAttribute
        , centerX
        ]
        { onPress = Just GetAndDisplayNewQrData
        , label =
            viewQr qrInfo
                |> el
                    [ Element.Background.color Configs.uiColors.lightGray
                    , px 256 |> width
                    , px 256 |> height
                    , Element.Border.glow Configs.uiColors.gray 13
                    ]
        }


nickIsValid : String -> Bool
nickIsValid nick =
    not (String.isEmpty nick)
        && not (String.contains "/" nick)
        && not (nick == ".")
        && not (nick == "..")
        && not (nick == "\u{0000}")


viewNickInput : NewLoginInfo -> Element Msg
viewNickInput newLoginInfo =
    let
        nick =
            String.trim newLoginInfo.nick
    in
    row [ spacing 8 ]
        [ Element.Input.text
            [ fillPortion 3 |> width
            , height fill
            , Element.Border.glow Configs.uiColors.gray 1
            , Element.Border.rounded 4
            ]
            { onChange = SetNewLoginNick
            , text = newLoginInfo.nick
            , placeholder = Nothing
            , label = "" |> Element.Input.labelHidden
            }
        , Element.Input.button
            [ fillPortion 1 |> width
            , height fill
            , Element.Border.shadow
                { offset = ( 1, 1 )
                , size = 2
                , blur = 2
                , color = Configs.uiColors.gray
                }
            , Element.Border.rounded 999
            , if nickIsValid nick then
                Configs.uiColors.blue |> Element.Background.color

              else
                Configs.uiColors.dimmedBlue |> Element.Background.color
            ]
            { onPress =
                if newLoginInfo.qr.display then
                    Just HideQr

                else if not (nickIsValid nick) then
                    Nothing

                else
                    Just GetAndDisplayNewQrData
            , label =
                (if newLoginInfo.qr.display then
                    text "隐藏"

                 else
                    text "登录"
                )
                    |> el
                        [ centerX
                        , centerY -- not sure if this has any effect
                        , if nickIsValid nick then
                            Configs.uiColors.lightGray |> EF.color

                          else
                            Configs.uiColors.gray |> EF.color
                        , EF.bold
                        ]
            }
        ]


viewControlPanel : Element Msg
viewControlPanel =
    column
        [ width fill
        , centerX
        , spacing 24
        ]
        [ viewUpnpServiceRestartButton
        , viewSearchEngineEntrance
        , viewDeviceControlButtons
        ]


viewDeviceControlButtons : Element Msg
viewDeviceControlButtons =
    row
        [ px 256 |> width
        , centerX
        , spacing 7
        ]
        [ viewDeviceRebootButton
        , viewDevicePowerOffButton
        ]


mkControlButton :
    { action : Maybe Msg
    , iconPath : String
    , iconDescription : String
    , btnName : String
    , bgColor : Element.Color
    , fgColor : Element.Color
    }
    -> Element Msg
mkControlButton cfg =
    Element.Input.button
        [ width fill
        , Element.Border.rounded 999
        , Element.Border.shadow
            { offset = ( 1, 1 )
            , size = 2
            , blur = 2
            , color = Configs.uiColors.gray
            }
        , centerX
        , cfg.bgColor |> Element.Background.color
        , padding 13
        ]
        { onPress = cfg.action
        , label =
            row
                [ width fill ]
                [ image
                    [ px 32 |> width
                    , px 32 |> height
                    , alignLeft
                    ]
                    { src = cfg.iconPath
                    , description = cfg.iconDescription
                    }
                    |> el [ fillPortion 1 |> width ]
                , text cfg.btnName
                    |> el
                        [ fillPortion 3 |> width
                        , EF.center
                        , EF.bold
                        , cfg.fgColor |> EF.color
                        ]
                ]
        }


viewDeviceRebootButton : Element Msg
viewDeviceRebootButton =
    mkControlButton
        { action = Just RequestDeviceReboot
        , iconPath = "/assets/reboot.svg"
        , iconDescription = "An arrow pointing to its own tail"
        , btnName = "重启"
        , bgColor = Configs.uiColors.gray
        , fgColor = Configs.uiColors.darkGray
        }


viewDevicePowerOffButton : Element Msg
viewDevicePowerOffButton =
    mkControlButton
        { action = Just RequestDevicePowerOff
        , iconPath = "/assets/power.svg"
        , iconDescription = "A 1 in front of a 0"
        , btnName = "关机"
        , bgColor = Configs.uiColors.darkGray
        , fgColor = Configs.uiColors.gray
        }


viewUpnpServiceRestartButton : Element Msg
viewUpnpServiceRestartButton =
    mkControlButton
        { action = Just RequestUpnpServiceRestart
        , iconPath = "/assets/redo.svg"
        , iconDescription = "Two arrows resembling a circle, pointing to each other's tail"
        , btnName = "重新扫描内容"
        , bgColor = Configs.uiColors.gray
        , fgColor = Configs.uiColors.darkGray
        }
        |> el [ px 200 |> width, centerX ]


viewSearchEngineEntrance : Element Msg
viewSearchEngineEntrance =
    mkControlButton
        { action = Just (OpenUrlInNewTab Configs.searchingSiteUrl)
        , iconPath = "/assets/looking-glass.svg"
        , iconDescription = "A looking glass"
        , btnName = "搜索资源"
        , bgColor = Configs.uiColors.white
        , fgColor = Configs.uiColors.darkGray
        }
        |> el [ px 200 |> width, centerX ]
