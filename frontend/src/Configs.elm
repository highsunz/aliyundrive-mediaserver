module Configs exposing (..)

import Element exposing (rgb255)


searchingSiteUrl : String
searchingSiteUrl =
    "https://yiso.fun/"


siteTitle : String
siteTitle =
    "控制台"


mountRoot : String
mountRoot =
    "/run/aliyundrive-mediaserver"


uiColors =
    { red = rgb255 0xDB 0xA3 0x9A
    , green = rgb255 0xC4 0xDF 0xAA
    , blue = rgb255 0x78 0x95 0xB2
    , dimmedGreen = rgb255 0xE7 0xF2 0xDC
    , dimmedRed = rgb255 0xEC 0xCF 0xCB
    , dimmedBlue = rgb255 0x98 0xB5 0xD2
    , white = rgb255 0xFE 0xFB 0xE9
    , lightGray = rgb255 0xF0 0xF0 0xF0
    , gray = rgb255 0xCC 0xCC 0xCC
    , darkGray = rgb255 0x33 0x33 0x33
    }


colorForStatus : Bool -> Element.Color
colorForStatus status =
    if status then
        uiColors.green

    else
        uiColors.red


dimmedColorForStatus : Bool -> Element.Color
dimmedColorForStatus status =
    if status then
        uiColors.dimmedGreen

    else
        uiColors.dimmedRed
